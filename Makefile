

usage:
	@echo "make (all|ssipt|log)"
all: ssipt log

ssipt:
	mkdir -p cgi
	mkdir -p etc
	mkdir -p docs
	if [ ! -r docs/index.html ]; then echo Hello >> docs/index.html; fi
log:
	mkdir log
